const url = 'https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.geojson';

// Perform a GET request to the query URL
d3.json(url, function(data) {
  // Once we get a response, send the data.features object to the createFeatures function
  createFeatures(data.features);
});
    // Create a map object
    const myMap = L.map("map", {
        center: [36.0255, -117.8206667],
        zoom: 3,
        // preferCanvas: true,
    });

function createFeatures(earthquakeData) {

    // Create a GeoJSON layer containing the features array on the earthquakeData object
    // Run the onEachFeature function once for each piece of data in the array
    var earthquakes = L.geoJSON(earthquakeData, {
        onEachFeature: curryOnEachFeature(myMap),
    });

    createMap(earthquakes, myMap);
    addLegend(myMap);
}

// Define a function we want to run once for each feature in the features array
// Give each feature a popup describing the place and time of the earthquake
function curryOnEachFeature(myMap) {
    return function(feature, layer) {
        if (feature.properties.type !== 'earthquake'){
            return;
        }

        const color = resolveColor(feature);
        const radius = resolveRadius(feature);
        const popup = resolvePopup(feature);
        const coors = resolveCoor(feature);

        L.circle(coors, {
            fillOpacity: 0.75,
            color: color,
            fillColor: color,
            radius: radius, 
        }).bindPopup(popup).addTo(myMap);
    }
}


function createMap(earthquakes, myMap) {
    // Adding tile layer
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery Â© <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/dark-v10',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: API_KEY
    }).addTo(myMap);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery Â© <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: API_KEY
    }).addTo(myMap);
}

function resolveCoor(e){
    const point = e.geometry.coordinates;
    return [
        point[1],
        point[0],
    ];
}

function resolvePopup(e){
    const point = e.geometry.coordinates;
    return "<h1>Earthquake!</h1> <hr> <h3>Magnitude: " 
        + e.properties.mag + "</h3>" + 
        `<h3>Latitude: ${point[1]}</h3>` + 
        `<h3>Longitude: ${point[0]}</h3>`  
        ;
}

function resolveRadius(e){
    const mag = e.properties.mag;
    // logarithmic scale is logarithmic
    return mag * mag * 1000;
}

function resolveColor(e){
    const mag = e.properties.mag;
    if (mag < 2){
        return 'blue';
    }
    if (mag < 3){
        return 'green';
    }
    if (mag < 4){
        return 'yellow';
    }
    if (mag < 5){
        return 'orange';
    }
    return 'red';
}

function addLegend(myMap) {
    var legend = L.control({ position: "bottomright" });
    legend.onAdd = function() {
        var div = L.DomUtil.create("div", "info legend");
        var limits = [
            'Less Than 2', 
            'Less Than 3', 
            'Less Than 4', 
            'Less Than 5',
            'Greater or Equal to 5',
        ];
        var colors = ['blue', 'green', 'yellow', 'orange', 'red'];
        var labels = [];

        // Add min & max
        var legendInfo = "<h1>Magnitude</h1>" +
            "<div class=\"labels\">" +
            "<div class=\"min\"></div>" +
            "<div class=\"max\"></div>" +
            "</div>";

        div.innerHTML = legendInfo;

        limits.forEach(function(limit, index) {
            labels.push("<li style=\"background-color: " + 
                colors[index] + 
                "\">" + 
                limits[index] + 
                "</li>");
        });

        div.innerHTML += "<ul>" + labels.join("") + "</ul>";
        return div;
    };

    // Adding legend to the map
    legend.addTo(myMap);
}